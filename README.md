Here are some worksamples.  *PLEASE NOTE*: this is a tiny sample, and are the simplest of examples because I had to be careful not to release any proprietary or private information.  Some items are sanitized and several are very old, but allowable to share because they are in reference to now obsolete and retired systems.

General Contents:

* centos-customer-generic_logrotation-v1.docx 
	- a word doc to implement log rotation
* gcgs-liferay-install-v1_3.docx 
	- a word doc to install liferay on a particular client's platforms
* LoadBalancerSuggestions.docx 
	- a word doc suggesting how a particular client may improve SLB functionality
* setupAutoAPT.sh 
	- a down&dirty script to setup AutoApt on an Ubuntu system.
* DPKGpostinstall-SeaborgConfig.sh 
	- a script to configure a system based upon its hostname/function
* ObsoleteELNKserviceArchitecture.htm 
	- an old (and thus not a trade secret) service diagram of a log aggregator.


