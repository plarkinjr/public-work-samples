#_____SEABORG-CONF-BEGIN____
# $Id: syslogconf.pas.frag,v 1.1 2009/10/08 15:24:26 pat.larkin Exp $
# $Header: /cvs/root/sysadmin/configuration/seaborg/dpkg/syslogconf.pas.frag,v 1.1 2009/10/08 15:24:26 pat.larkin Exp $

                                                    
# Send important stuff to Sys Admin loghost          
#*.info;kern.none;mail.none             @logwatch.admin.pas.earthlink.net
#kern.debug;mail.err                    @logwatch.admin.pas.earthlink.net


# If you use %HOSTNAME% string, postinstall (hostifyFile.sh)
# will replace it with the value returned by `hostname`
# That string again is PercentHOSTNAMEPercent, or
# %.HOSTNAME.% (but without the dots)
# example
#local7.debug		/ms/logs/uberlogs/uberlog.%HOSTNAME%

#_____SEABORG-CONF-END______
