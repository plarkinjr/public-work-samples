#_____SEABORG-CONF-BEGIN____
# $Id: syslogconf.atl.frag,v 1.2 2009/10/21 19:19:13 pat.larkin Exp $
# $Header: /cvs/root/sysadmin/configuration/seaborg/dpkg/syslogconf.atl.frag,v 1.2 2009/10/21 19:19:13 pat.larkin Exp $

# Send important stuff to Sys Admin loghost          
#*.info;kern.none;mail.none             @loghost.s.atl.sa.earthlink.net
#kern.debug;mail.err                    @loghost.s.atl.sa.earthlink.net


# If you use %HOSTNAME% string, postinstall (hostifyFile.sh)
# will replace it with the value returned by `hostname`
# That string again is PercentHOSTNAMEPercent, or
# %.HOSTNAME.% (but without the dots)
# example
#local7.debug		/ms/logs/uberlogs/uberlog.%HOSTNAME%

#_____SEABORG-CONF-END______
