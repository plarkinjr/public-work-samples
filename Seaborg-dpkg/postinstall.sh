#!/bin/sh 
# $Id: postinstall.sh,v 1.45 2011/08/17 13:52:25 pat.larkin Exp $
# 
# This is the seaborg config dpkg pre-install script.

#

echo "Now running $0"

cd /ms/svc/seaborg/dpkg

# First, find out what my role is:
ROLE=`./learn-my-role.sh`
SITE=`./learn-my-site.sh`

echo "My name is `/bin/hostname` ... "
echo "and my Seaborg function is: $ROLE"
echo "and I live in $SITE "
echo "================= so let's get to work...."


# Set up lib paths:
crle -l /ms/lib/pcre/lib/:/ms/lib/libxml2/lib/:/ms/lib/libxslt/lib:/ms/local/ssl/lib:/usr/lib:/lib

#create a file that seaborg likes to write alarms to..... must match whats in other config files
touch /var/tmp/bigbroalarms
chmod 666 /var/tmp/bigbroalarms




###############  SysLog
# Start with a clean common file
#echo "drop in a base file for /etc/syslog.conf ..."
#cp syslogconf.base /etc/syslog.conf
# Add the fragment file from this dpkg to the system file
echo "  add site-specific stuff to /etc/syslog.conf ..."
./edfile-tool.sh -a syslogconf.$SITE.frag /etc/syslog.conf
# replace %HOSTNAME% in it with the machine's name
echo "  'hostifying' /etc/syslog.conf with my hostname ..."
./hostifyFile.sh /etc/syslog.conf
# Create any missing log files
echo "  creating any logfiles that syslog will want to write to..."
./mklogfiles.sh
# bounce syslogd to get the changes
echo "  bouncing syslogd to re-read the conf file..."
kill -HUP `cat /etc/syslog.pid`


# Add a basic crontab for all

  ./edfile-tool.sh -r crontab.obsolete.frag /var/spool/cron/crontabs/root
  ./edfile-tool.sh -a crontab.ALL.frag /var/spool/cron/crontabs/root





#  Do site-specific stuff
##### USE $SITE variable instead
case $SITE in
  qts)
	# Do QTS-specific stuff here
	# create mountpoints
	./edfile-tool.sh -a vfstab.qts.frag /etc/vfstab 

	# resolv.conf - cuz nobody else does it.
	#cp resolv.conf.qts /etc/resolv.conf

	#rm /etc/init.d/staticroutes
	#cp init.staticroutes.qts /etc/init.d/staticroutes
	#sh /etc/init.d/staticroutes start
	;;
  pas)
	# Do Pasadena-specific stuff here
	# create mountpoints
	umount /ms/svc/seaborg/share
	mkdir -p /ms/svc/seaborg/share
	# Retired
	#mkdir -p /ms/svc/seaborg/share/sbfiler-ransom
	#mkdir -p /ms/svc/seaborg/share/sbfiler-gabar
	#mkdir -p /ms/svc/seaborg/share/sbfiler-oomph
	mkdir -p /ms/uberlogs/A 
	mkdir -p /ms/uberlogs/B 
	mkdir -p /ms/uberlogs/C 
	mkdir -p /ms/uberlogs/D
	./edfile-tool.sh -a vfstab.pas.frag /etc/vfstab 

	# resolv.conf - cuz nobody else does it.
	cp resolv.conf.pas /etc/resolv.conf

	rm /etc/init.d/staticroutes.seaborg
	cp init.staticroutes.pas /etc/init.d/staticroutes.seaborg
	sh /etc/init.d/staticroutes.seaborg start
	;;

  atl)
	# Do Atlanta-specific stuff here
	# Add static routes
	rm /etc/init.d/staticroutes.seaborg
	cp init.staticroutes.atl /etc/init.d/staticroutes.seaborg
	sh /etc/init.d/staticroutes.seaborg start

	# create mountpoints
	umount /ms/svc/seaborg/share
	mkdir -p /ms/svc/seaborg/share
	mkdir -p /ms/svc/seaborg/share/seaborg-node1
	# retired
	#mkdir -p /ms/svc/seaborg/share/sbfiler-ransom
	#mkdir -p /ms/svc/seaborg/share/sbfiler-gabar
	#mkdir -p /ms/svc/seaborg/share/sbfiler-oomph
	mkdir -p /ms/uberlogs/A 
	mkdir -p /ms/uberlogs/B 
	mkdir -p /ms/uberlogs/C 
	mkdir -p /ms/uberlogs/D
	mkdir -p /ms/deploy
	mkdir -p /ms/home

	# put the entries for above into vfstab
	./edfile-tool.sh -a vfstab.atl.frag /etc/vfstab 

	# resolv.conf - cuz nobody else does it.
	cp resolv.conf.atl /etc/resolv.conf
	;;
     *)
	echo "Hmmm, I dont know what site I am."
	echo "leaving much configuration out"
	;;
esac

# now, mount them all
mount -a

# Do personality/case-specifics
case $ROLE in
  Cringer) echo "I'm a $ROLE" 
	# Build a cringer.conf XML file
	./logfiles2cringerconf.sh ./logfiles.`hostname` ../etc/cringer.conf
	# Setup inittab entry
	./edfile-tool.sh -a inittab.cringer.frag /etc/inittab
  	;;
  Indexer) echo "I'm a $ROLE" 
	# Build the wrindex-cfg.xml file
	./buildwrindex-conf.sh ../etc/wrindex-cfg.xml
	#  Setup RC scripts
	cp ./init.wrindex /etc/init.d/wrindex
	chmod 555 /etc/init.d/wrindex
	ln -s /etc/init.d/wrindex /etc/rc2.d/S90wrindex
  	;;
  Searcher) echo  "I'm a $ROLE" 
     	./edfile-tool.sh -a crontab.web.frag /var/spool/cron/crontabs/root
	cp ./init.httpd /etc/init.d/httpd
	chmod 555 /etc/init.d/httpd
	ln -s /etc/init.d/httpd /etc/rc2.d/S95httpd
  	;;
  Database) echo  "I'm a $ROLE" 
     #  These have to run on one machine, DB sounds as good as any
        ./edfile-tool.sh -a crontab.db.frag /var/spool/cron/crontabs/root
	#  Setup RC scripts
	cp ./init.mysql /etc/init.d/mysql
	chmod 555 /etc/init.d/mysql
	ln -s /etc/init.d/mysql /etc/rc2.d/S90mysql
  	;;
  unknown) echo "I don't know WHAT the heck I'm supposed to do!" ;;
  *) echo "Nobody told me what I'm supposed to do!!!" ;;
esac

# Final global stuff
crontab /var/spool/cron/crontabs/root

exit 0
  
