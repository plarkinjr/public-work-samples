#!/bin/sh
# $Id: learn-my-site.sh,v 1.3 2011/08/17 14:13:07 pat.larkin Exp $
# $Header: /cvs/root/sysadmin/configuration/seaborg/dpkg/learn-my-site.sh,v 1.3 2011/08/17 14:13:07 pat.larkin Exp $
# 
# This script obtains the machine's hostname, and defines
# a variable for the machine's function

# make environment variable stick
set -a

HOSTNAME=`/bin/hostname`

# Get site info
case $HOSTNAME in
 *.qe.*) SITE=qe ;;
 *.qts.*) SITE=qts ;;
 *.pas.*) SITE=pas ;;
 *.atl.*) SITE=atl ;;
  *) SITE=unknown  ;;
esac

echo $SITE


