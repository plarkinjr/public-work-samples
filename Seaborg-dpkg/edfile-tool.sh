#!/bin/sh
# $Id: edfile-tool.sh,v 1.1 2006/06/05 19:45:39 pat.larkin Exp $
# 
# This script will remove a portion of a file
# and (optionally) add another file to it

# Shamelessly stolen example from dynadump
#crontab -l 2>/dev/null | sed -e '/^#__DDBEGIN__$/,/^#__DDEND__$/d' > /tmp/$$crontab || exit 2
#cat ${CRONDIR}/dynadump.cron >> /tmp/$$crontab
#crontab /tmp/$$crontab 


# Usage:
#  edfile tool -[r|a]  filespec_for_file_fragment filespec_for_file_to_modify
#	where "r" means REMOVE and "a" means ADD
# Get opts

if [ $# != 3 ] ; then
	echo "Doh!"
	exit 1
fi

OPT=$1
FRAG=$2
FILE=$3

# Sanity checks:
if [ ! -f $FRAG ] ; then
	echo "No such fragment file $FRAG"
	exit 1
fi
if [ ! -f $FILE ] ; then
	echo "No such file $FILE"
	exit 1
fi
case $OPT in
	"-a")  TYPE=a ;;
	"-r")  TYPE=r ;;
	*) echo "unrecognized option $OPT" ; exit 1 ;;
esac

# Now, get delimiters
BEGIN=`head -1 $FRAG`
END=`tail -1 $FRAG`

# Now, backup the file and start operating
cp $FILE $FILE.bak
cat $FILE.bak | sed -e '/^'$BEGIN'$/,/^'$END'$/d' > $FILE.work || exit 2
if [ $TYPE = a ] ; then
	# add the fragment
	cat $FRAG >> $FILE.work
fi

# Always put the file back
cp $FILE.work $FILE

