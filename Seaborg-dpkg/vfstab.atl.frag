#_____SEABORG-CONF-BEGIN____
# $Id: vfstab.atl.frag,v 1.5 2011/04/11 14:29:39 pat.larkin Exp $
#
# The seaborg data filer
seaborg-node1.p.atl.sa:/vol/seabrgdr1   - /ms/svc/seaborg/share/seaborg-node1  nfs  - yes   rw,bg,hard,intr,vers=3,proto=tcp,timeo=1000,rsize=32768,wsize=32768

# Retired
#sbfiler-gabar:/vol/seaborg              - /ms/svc/seaborg/share/sbfiler-gabar  nfs  - yes   rw,bg,hard,intr,vers=3,proto=tcp,timeo=1000,rsize=32768,wsize=32768
#sbfiler-oomph:/vol/seaborg              - /ms/svc/seaborg/share/sbfiler-oomph  nfs  - yes   rw,bg,hard,intr,vers=3,proto=tcp,timeo=1000,rsize=32768,wsize=32768
#
# Where other services log to:
lognas-omega-n1:/vol/mailadmin1/maillogs  - /ms/uberlogs/A   nfs  -  yes rw,bg,intr,vers=3,proto=udp,acdirmax=0,nosuid 
callisto.pri.atl.earthlink.net:/vol/logs/uberlogs              - /ms/uberlogs/B   nfs  -  yes rw,bg,hard,intr,vers=3,proto=tcp,timeo=1000
pollux.pri.atl.earthlink.net:/vol/logs/uberlogs                - /ms/uberlogs/C   nfs  -  yes rw,bg,hard,intr,vers=3,proto=tcp,timeo=1000
metis-200.p.atl.sa.earthlink.net:/vol/logs/uberlogs            - /ms/uberlogs/D   nfs  -  yes rw,bg,hard,intr,vers=3,proto=tcp,timeo=1000
# Poodle replaced by omega
#lognas-poodle.p.atl.sa.earthlink.net:/vol/mailadmin1/maillogs  - /ms/uberlogs/A   nfs  -  yes rw,bg,intr,vers=3,proto=udp,acdirmax=0,nosuid 
#
# ...and because nobody else is going to handle these:
nimitz.pri.atl.earthlink.net:/vol/users/home                   - /ms/home         nfs - yes vers=3,proto=udp,intr,rw,acdirmin=60,bg,nosuid
nimitz.pri.atl.earthlink.net:/vol/deploy                       - /ms/deploy       nfs - yes vers=3,proto=udp,intr,rw,acdirmin=60,bg,nosuid

#_____SEABORG-CONF-END______
