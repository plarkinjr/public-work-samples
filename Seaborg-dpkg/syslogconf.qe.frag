#_____SEABORG-CONF-BEGIN____
# $Id: syslogconf.qe.frag,v 1.1 2009/10/08 15:24:26 pat.larkin Exp $
# $Header: /cvs/root/sysadmin/configuration/seaborg/dpkg/syslogconf.qe.frag,v 1.1 2009/10/08 15:24:26 pat.larkin Exp $

                                                    
# Send important stuff to Sys Admin loghost          
#*.info;kern.none;mail.none             @loghost.s.atl.sa.earthlink.net
#kern.debug;mail.err                    @loghost.s.atl.sa.earthlink.net


# If you use %HOSTNAME% string, postinstall (hostifyFile.sh)
# will replace it with the value returned by `hostname`
# That string again is PercentHOSTNAMEPercent, or
# %.HOSTNAME.% (but without the dots)
# example
#local7.info		/ms/logs/uberlogs/uberlog.%HOSTNAME%

#_____SEABORG-CONF-END______
