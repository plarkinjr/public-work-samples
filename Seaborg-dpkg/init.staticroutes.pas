#!/bin/sh
# $Id: init.staticroutes.pas,v 1.4 2009/11/19 15:29:23 pat.larkin Exp $
#
#  Add static routes.

case "$1" in
    start)
        #route add 192.168.200.0 10.121.36.1
        #route add 10.121.0.0/16 10.121.36.1     
	# Mouse needed this
        #route add 10.4.120.0/24 10.121.67.1     
	# DR boxes need this
        route add 10.4.120.0/24 10.161.67.1
        ;;
    stop)
        #route delete 192.168.200.0 10.121.36.1
        #route delete 10.121.0.0/16 10.121.36.1
        #route delete 10.4.120.0/24 10.121.67.1     
        route delete 10.4.120.0/24 10.161.67.1
        ;;
    *)
        echo "Usage: $0 { start | stop }"
        ;;
esac

