#!/bin/sh
# $Id: buildwrindex-conf.sh,v 1.15 2010/10/04 17:25:18 pat.larkin Exp $

# This creates the wrindex-cfg.xml from scratch, and sets up stuff

#  What file do we write the xml configs to?
WRINDCONF=$1
# Figure out who I am
HOSTNAME=`hostname`
SITE=`./learn-my-site.sh`

# Now that DR was killed, the filer in PAS was moved to ATL
# and is replacing oomph and gabar as the one filer there.
case $SITE in
	# PAS has one filer:
	pas) FILER=sbfiler-ransom ;;
	# ATL has two:
	atl)
		# Figure out which filer to go to based on hsotname length::
		NLEN=`echo  $HOSTNAME | cut -f1 -d. | cut -f2 -d\- | wc -c`
		case `echo $NLEN` in
        		3) FILER=seaborg-node1 ;;
        		4) FILER=seaborg-node1 ;;
        		5) FILER=seaborg-node1 ;;
        		6) FILER=seaborg-node1 ;;
        		7) FILER=seaborg-node1 ;;
        		8) FILER=seaborg-node1 ;;
        		9) FILER=seaborg-node1 ;;
        		*) FILER=seaborg-node1 ;;
		esac
		;;
	qe) FILER=localhost ;;
	*) FILER=localhost ;;
esac

#  Where's the NFS mount?  Mount it
SHARE="/ms/svc/seaborg/share/$FILER" ; mount $SHARE
#  Define this machines index and datastore, and create it
INDEX="$SHARE/$HOSTNAME" ; mkdir -p $INDEX
DATASTORE="$SHARE/$HOSTNAME" ; mkdir -p $DATASTORE



#  What file will bigbrother watch?
BIGBRO="/var/tmp/bigbroalarms" ; touch $BIGBRO

#  Something bad happened, show the user his error
if [ "$WRINDCONF" = "" ] ; then
	echo "Usage:  $0 outfile" ; exit 1
fi


echo "$0 $LOGFILES $WRINDCONF \c"
echo "<?xml version='1.0' ?>" > $WRINDCONF
echo "<!-- Machine Generated - do not modify -->" >>$WRINDCONF
echo "" >> $WRINDCONF
echo "<!-- Generated `date` in `pwd` from: -->" >> $WRINDCONF
echo "<!-- $0  $WRINDCONF -->" >> $WRINDCONF

echo "" >> $WRINDCONF
echo "<config>" >> $WRINDCONF
echo "" >> $WRINDCONF

echo "        <debuglevel>5</debuglevel>" >> $WRINDCONF
echo "        <bigbro>$BIGBRO</bigbro> <!-- bigbro watches syslog -->" >> $WRINDCONF
echo "        " >> $WRINDCONF
echo "        <connections>" >> $WRINDCONF
echo "                <maxinbound>5</maxinbound>" >> $WRINDCONF
echo "                <listenport>7600</listenport>" >> $WRINDCONF
echo "        </connections>" >> $WRINDCONF
echo "" >> $WRINDCONF
echo "        <datastore>" >> $WRINDCONF
echo "            <path>$DATASTORE</path>" >> $WRINDCONF
echo "                <maxsize>1500M</maxsize>" >> $WRINDCONF
echo "                <commitinterval>17</commitinterval>" >> $WRINDCONF
echo "                <buffersize>16M</buffersize>" >> $WRINDCONF
echo "        </datastore>" >> $WRINDCONF
echo "" >> $WRINDCONF
echo "        <index>" >> $WRINDCONF
echo "                <path>$INDEX</path>" >> $WRINDCONF
echo "                <keysperpage>3068</keysperpage>" >> $WRINDCONF
echo "                <hashbuckets>65536</hashbuckets>" >> $WRINDCONF
echo "                <maxsize>1850M</maxsize>" >> $WRINDCONF
echo "                <commitinterval>120</commitinterval>" >> $WRINDCONF
echo "                <metacommitinterval>60</metacommitinterval>" >> $WRINDCONF
echo "                <buffersize>1500M</buffersize>" >> $WRINDCONF
echo "        </index>" >> $WRINDCONF
echo "" >> $WRINDCONF
echo "</config>" >> $WRINDCONF
echo "" >> $WRINDCONF
echo "<!-- Machine Generated - do not modify -->" >>$WRINDCONF
echo "" >> $WRINDCONF
echo "<!-- Generated `date` in `pwd` from: -->" >> $WRINDCONF
echo "<!-- $0 $WRINDCONF -->" >> $WRINDCONF


echo "....done."
