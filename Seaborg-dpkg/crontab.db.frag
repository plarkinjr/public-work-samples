#_____SEABORG-CONF-DB-BEGIN____
# $Id: crontab.db.frag,v 1.6 2011/04/07 14:03:17 pat.larkin Exp $

# Run Dyson in the session query purge mode. This will remove all expired
# search query sessions from the database (usually about 2 or 3 days old --
# see web-config.php for details)
#
# This needs to be run on only one web server at off-peak hours.

13 3 * * * /ms/svc/seaborg/bin/dyson.php -p

# Run Dyson in the expunge mode which will eradicate old stale data that has
# exceeded its usefulness. Typically, data and index files older than 30 days
# are deleted.
#  See  SEABORG_EXPUNGE_FILES_AFTER_DAYS in seaborg/etc/web-config.php
#
# This should be run weekly at weird, off-peak hours,

52 3 * * * /ms/svc/seaborg/bin/dyson.php -e


#_____SEABORG-CONF-DB-END______
