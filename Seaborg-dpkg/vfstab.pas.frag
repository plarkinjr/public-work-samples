#_____SEABORG-CONF-BEGIN____
# $Id: vfstab.pas.frag,v 1.14 2010/10/04 14:43:25 pat.larkin Exp $
#
#sbfiler-ransom.p:/vol/seabrgdr1    - /ms/svc/seaborg/share/sbfiler-ransom  nfs  - yes   rw,bg,hard,intr,vers=3,proto=tcp,timeo=1000,rsize=32768,wsize=32768
#
#
# Where other services log to:
#      (DR machines need static route to chapin )
#   (and a Pocket network Router )
chapin-gp.pocket:/vol/mailadm1/maillogs    - /ms/uberlogs/A   nfs  -  yes rw,bg,intr,vers=3,proto=tcp,acdirmax=0,nosuid 
orkin-e4c.it:/vol/misc/ubergrep            - /ms/uberlogs/B   nfs  -  yes rw,bg,hard,intr,vers=3,proto=tcp,timeo=1000
orkin-e4c.it:/vol/misc/directpc.ubergrep   - /ms/uberlogs/C   nfs  -  yes rw,bg,hard,intr,vers=3,proto=tcp,timeo=1000
orkin-e4c.it:/vol/auth/radius/uberlogs     - /ms/uberlogs/D   nfs  -  yes rw,bg,hard,intr,vers=3,proto=tcp,timeo=1000
#
#_____SEABORG-CONF-END______
