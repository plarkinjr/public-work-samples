#!/bin/sh
# $Id: hostifyFile.sh,v 1.1 2009/10/08 15:46:20 pat.larkin Exp $
# $Header: /cvs/root/sysadmin/configuration/seaborg/dpkg/hostifyFile.sh,v 1.1 2009/10/08 15:46:20 pat.larkin Exp $
# 
# This script will replace the string %HOSTNAME% in a file
# with the machine's hostname

# Shamelessly stolen example from dynadump
#crontab -l 2>/dev/null | sed -e '/^#__DDBEGIN__$/,/^#__DDEND__$/d' > /tmp/$$crontab || exit 2
#cat ${CRONDIR}/dynadump.cron >> /tmp/$$crontab
#crontab /tmp/$$crontab 


# Usage:
#  hostifyFile.sh filename

if [ $# != 1 ] ; then
	echo "Doh!"
	exit 1
fi

FILE=$1
HOSTNAME=`hostname`

if [ ! -f $FILE ] ; then
	echo "No such file $FILE"
	exit 1
fi

if [ -z "$HOSTNAME" ] ; then
	echo "Cannot determine hostname"
	exit 1
fi


# Now, backup the file and start operating
cp $FILE $FILE.bak-$$

#
#cat $FILE.bak | sed -e '/^'$BEGIN'$/,/^'$END'$/d' > $FILE.work || exit 2
#
cat $FILE.bak-$$ | sed -e 's/%HOSTNAME%/'$HOSTNAME'/g' > $FILE.work || exit 2


# Always put the file back
cp $FILE.work $FILE
# and cleanup
rm $FILE.work $FILE.bak-$$

