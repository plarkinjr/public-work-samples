#!/bin/sh
# $Id: learn-my-role.sh,v 1.4 2011/05/10 16:37:54 pat.larkin Exp $
# 
# This script obtains the machine's hostname, and defines
# a variable for the machine's function

# make environment variable stick
set -a

HOSTNAME=`/bin/hostname`


#  Radius (and any others you add) are assumed to be cringer

case $HOSTNAME in

  radius-*|other-*) ROLE=Cringer ;;
  ugrepcat-*|sbcat-*|mouse*) ROLE=Cringer  ;;
  ugrepindex-*|sbindex-*) ROLE=Indexer ;;
  ugrepweb-*|sbweb-*) ROLE=Searcher ;;
  ugrepwrite-*|sbdb-*) ROLE=Database ;;
  *) ROLE=unknown  ;;

esac
echo $ROLE


