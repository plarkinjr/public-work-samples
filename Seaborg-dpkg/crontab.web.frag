#_____SEABORG-CONF-WEB-BEGIN____
# $Id: crontab.web.frag,v 1.7 2010/03/30 17:01:40 pat.larkin Exp $

# Run Dyson in metadata mode every minute. This will rebuild the index cache
# on the system.
#
# This needs to be run by EVERY web server.

* * * * * /ms/svc/seaborg/bin/dyson.php -m 

# Run Dyson in metadata mode every minute. This will rebuild the index cache
# on the system.
#
# This needs to be run by EVERY web server.

* * * * * /ms/svc/seaborg/bin/dyson.php -x

# Randomly restart httpd
#  Not really needed any more after PHP fix
#12 2,8,14,20 * * * /ms/svc/seaborg/etc/RandomRestart-httpd


#_____SEABORG-CONF-WEB-END______
