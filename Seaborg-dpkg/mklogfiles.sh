#!/bin/sh
# $Id: mklogfiles.sh,v 1.1 2009/10/08 15:46:20 pat.larkin Exp $
# $Header: /cvs/root/sysadmin/configuration/seaborg/dpkg/mklogfiles.sh,v 1.1 2009/10/08 15:46:20 pat.larkin Exp $
# 
# This script will scan syslog.conf and touch (create) anything that doesnt exist

##### Consider using this to 'hostify' syslog.conf

grep "^[a-z]" /etc/syslog.conf | {
while read line ; do
        FILE=`echo $line | awk '{ print $2 }' | grep "^/" `
	if [ ! -w $FILE ]; then
                touch $FILE
		chmod 644 $FILE
        fi
done
}

