#!/bin/sh -x
# $Id: postremove.sh,v 1.3 2006/06/13 19:03:09 pat.larkin Exp $
# 
# This is the seaborg config dpkg pre-install script.
#

cd /ms/svc/seaborg/dpkg

# First, find out what my role is:
ROLE=`./learn-my-role.sh`

echo "Nothing to do in $0"
