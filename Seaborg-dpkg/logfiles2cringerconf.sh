#!/bin/sh
# $Id: logfiles2cringerconf.sh,v 1.22 2011/08/15 18:41:22 pat.larkin Exp $

# This converts a simple "logfiles" file to a cringer conf in XML

#LOGFILES=logfiles
LOGFILES=$1
#CRINGER=/tmp/cringer.conf
CRINGER=$2

if [ "$CRINGER" = "" ] ; then
	echo "Usage:  $0 infile outfile" ; exit 1
fi

#  Create the top header and global config entries

echo "$0 $LOGFILES $CRINGER \c"
echo "<?xml version='1.0' ?>" > $CRINGER
echo "<!-- Machine Generated - do not modify -->" >>$CRINGER
echo "" >> $CRINGER
echo "<!-- Generated `date` in `pwd` from: -->" >> $CRINGER
echo "<!-- $0 $LOGFILES $CRINGER -->" >> $CRINGER

echo "" >> $CRINGER
echo "<config>" >> $CRINGER
echo "" >> $CRINGER
echo "<!--  Global Settings  -->" >> $CRINGER
echo "    <debuglevel>10</debuglevel>" >> $CRINGER
echo "    <temppath>/ms/svc/seaborg/var</temppath>" >> $CRINGER
echo "    <pluginpath>/ms/svc/seaborg/lib/cringer-plugins</pluginpath>" >> $CRINGER
echo "    <bigbro>/var/tmp/bigbroalarms</bigbro> <!-- bigbro watches syslog -->" >> $CRINGER
echo "" >> $CRINGER


##### Obtain the server name
SERVERS=`grep SERVER= $LOGFILES | cut -f2 -d= | paste -s - `
##### Now write them out
for SERVER in $SERVERS ; do
	echo "    <connect fqdn='$SERVER'>" >> $CRINGER
	echo "        <port>7600</port>" >> $CRINGER
	echo "        <numofconns>1</numofconns>" >> $CRINGER
	echo "    </connect>" >> $CRINGER
done

#### This is the old way of doing it.
#  echo "    <connect fqdn='sbindex-ivory.atl.sa'>" >> $CRINGER
#  echo "        <port>7600</port>" >> $CRINGER
#  echo "        <numofconns>1</numofconns>" >> $CRINGER
#  echo "    </connect>" >> $CRINGER

#  echo "    <connect fqdn='sbindex-carolina.atl.sa'>" >> $CRINGER
#  echo "        <port>7600</port>" >> $CRINGER
#  echo "        <numofconns>1</numofconns>" >> $CRINGER
#  echo "    </connect>" >> $CRINGER

#  echo "    <connect fqdn='sbindex-dutch.atl.sa'>" >> $CRINGER
#  echo "        <port>7600</port>" >> $CRINGER
#  echo "        <numofconns>1</numofconns>" >> $CRINGER
#  echo "    </connect>" >> $CRINGER

echo "" >> $CRINGER
echo "" >> $CRINGER
echo "" >> $CRINGER

# Now, read the input file and build the XML
grep "^[a-z]"  $LOGFILES | {
while read line ; do

TYPE=`echo $line | cut -f1 -d:`
FILE=`echo $line | cut -f2 -d:`
ZONE=`echo $line | cut -f3 -d:`

case $TYPE in 
	radius)  
		echo "" >> $CRINGER
		echo "<file uri=\"$FILE\">" >> $CRINGER
		echo "	<plugin>uplf</plugin>" >> $CRINGER
		echo "	<serviceid>1</serviceid>" >> $CRINGER
		echo "	<tz>$ZONE</tz>" >> $CRINGER
		echo "	<key>Incoming-User-Name</key>" >> $CRINGER
		echo "	<key>Client-IP</key>" >> $CRINGER
		echo "	<key>NAS-IP-Address</key>" >> $CRINGER
		echo "	<key>Framed-IP-Address</key>" >> $CRINGER
		echo "	<key>{Authenticated-User-Name</key>" >> $CRINGER
		echo "	<key>ani</key>" >> $CRINGER
		echo "	<key>dnis</key>" >> $CRINGER
		echo "</file>" >> $CRINGER
		echo "" >> $CRINGER


	;;
	mail)
		echo "" >> $CRINGER
		echo "<file uri=\"$FILE\">" >> $CRINGER
		echo "	<plugin>mail</plugin>" >> $CRINGER
		echo "	<serviceid>2</serviceid>" >> $CRINGER
		echo "	<tz>$ZONE</tz>" >> $CRINGER
		echo "</file>" >> $CRINGER
		echo "" >> $CRINGER
	;;

	imap|pop)
		echo "" >> $CRINGER
		echo "<file uri=\"$FILE\">" >> $CRINGER
		echo "	<plugin>uplf</plugin>" >> $CRINGER
		echo "	<serviceid>2</serviceid>" >> $CRINGER
		echo "	<tz>$ZONE</tz>" >> $CRINGER
		echo "	<key>mailbox</key>" >> $CRINGER
		echo "</file>" >> $CRINGER
		echo "" >> $CRINGER


	;;
	other)
		echo "" >> $CRINGER
		echo "<file uri=\"$FILE\">" >> $CRINGER
		echo "	<plugin>generic</plugin>" >> $CRINGER
		echo "	<serviceid>3</serviceid>" >> $CRINGER
		echo "	<tz>$ZONE</tz>" >> $CRINGER
		echo "	<key n='ip'>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}</key>" >> $CRINGER
		echo "	<key n='email'>[\w_\.\-]+[\w_\.\-\+]*\@[\w\-]+\.+[\w]{2,6}+</key>" >> $CRINGER
		echo "</file>" >> $CRINGER
		echo "" >> $CRINGER
	;;
	*) echo "Don't know what to do with this line:"
		echo $line
	;;
esac



echo ".\c"

done

echo "" >> $CRINGER
echo "</config>" >> $CRINGER
echo "" >> $CRINGER
echo "<!-- Machine Generated - do not modify -->" >>$CRINGER
echo "" >> $CRINGER
echo "<!-- Generated `date` in `pwd` from: -->" >> $CRINGER
echo "<!-- $0 $LOGFILES $CRINGER -->" >> $CRINGER
}
echo "done."
