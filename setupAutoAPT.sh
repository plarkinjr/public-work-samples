#!/bin/sh

#  plarkin@xtivia.com  - August 2015
#### This script will setup the "unattended-upgrades"
#### process for ubuntu/debian systems.
####  (tested on Ubuntu14

### 	You may want to modify the update options
### 	below within the EOF statements, such as
###	the ability to auto reboot (even at a certain time)

if [ `id -u` != "0" ] ; then
	echo "Must be Superuser!"
	exit 1
fi

echo "!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!"
echo "!  You may be asked several questions during this process.       !"
echo "!  the answers are along the lines of:                           !"
echo "!    *   your 'mail name' - which is the machine's name          !"
echo "!    *   who you really get mail to root and postmaster?         !"
echo "!    *   you are a mail SATTELITE                                ! "
echo "!    *   your SMTP Relay is probably smtp.austin.xtivia.com      !"
echo "!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!"


apt-get -y install  unattended-upgrades bsd-mailx update-notifier-common


# Make the "root" user's email have a useful/pretty "From:" header
FQDN=`nslookup \`hostname\` | grep ^Name: | awk '{ print $2 }'`
if [ "$FQDN" = "" ] ; then
	FQDN=`hostname`
fi
chfn -f "ROOT on $FQDN" -r "Systems Engineering" -w 512 -o "portal_admins@xtivia.com" root

echo "~~~~~~~~~~~~~~~~~ setting up /etc/apt/apt.conf.d/20auto-upgrades"
cp /etc/apt/apt.conf.d/20auto-upgrades /etc/apt/apt.conf.d/bak$$-20auto-upgrades
cat << EOF > /etc/apt/apt.conf.d/20auto-upgrades
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Unattended-Upgrade "1";
APT::Periodic::RandomSleep "1";
APT::Periodic::AutocleanInterval "4";
APT::Periodic::Verbose "1";
#  - Send report mail to root
#      0:  no report             (or null string)
#      1:  progress report       (actually any string)
#      2:  + command outputs     (remove -qq, remove 2>/dev/null, add -d)
#      3:  + trace on    

EOF


echo "~~~~~~~~~~~~~~~~~ setting up /etc/apt/apt.conf.d/50unattended-upgrades"
cp /etc/apt/apt.conf.d/50unattended-upgrades /etc/apt/apt.conf.d/bak$$-50unattended-upgrades
cat << EOF > /etc/apt/apt.conf.d/50unattended-upgrades
// Automatically upgrade packages from these (origin:archive) pairs
Unattended-Upgrade::Allowed-Origins {
        "\${distro_id} stable";
        "\${distro_id}:\${distro_codename}-security";
        "\${distro_id}:\${distro_codename}-updates";
//      "\${distro_id}:\${distro_codename}-proposed";
//      "\${distro_id}:\${distro_codename}-backports";
};

// List of packages to not update (regexp are supported)
Unattended-Upgrade::Package-Blacklist {
//      "vim";
//      "libc6";
};

// Send email to this address for problems or packages upgrades
// If empty or unset then no email is sent, make sure that you
// have a working mail setup on your system. A package that provides
// 'mailx' must be installed. E.g. "user@example.com"
Unattended-Upgrade::Mail "portal_admins@xtivia.com";

// Set this value to "true" to get emails only on errors. Default
// is to always send a mail if Unattended-Upgrade::Mail is set
Unattended-Upgrade::MailOnlyOnError "true";

// Do automatic removal of new unused dependencies after the upgrade
// (equivalent to apt-get autoremove)
Unattended-Upgrade::Remove-Unused-Dependencies "true";

// Automatically reboot *WITHOUT CONFIRMATION*
//  if the file /var/run/reboot-required is found after the upgrade 
//Unattended-Upgrade::Automatic-Reboot "false";

// If automatic reboot is enabled and needed, reboot at the specific
// time instead of immediately
//  Default: "now"
//Unattended-Upgrade::Automatic-Reboot-Time "02:00";

EOF


echo "~~~~~~~~~~~~~~~~~ "
echo "Would you like to run thru the postfix mail config again? [y/*]" ; read mailc
if [ "$mailc" = y ] ; then
	dpkg-reconfigure -plow postfix
fi
exit
